package net.xeill.elpuig;

class Marcador {

  Colores colores = new Colores();

  int maquina = 0;
  int persona = 0;
  int empate = 0;

  public void suma(String opcion){
    if(opcion.equals("persona")){
      persona++;
    } if(opcion.equals("maquina")){
      maquina++;
    } if (opcion.equals("empate")){
      empate++;
    }
  }

  public void imprimir(boolean print_empate){
    System.out.println(colores.PURPLE + " ___________________ " + colores.RESET);
    System.out.println(colores.PURPLE + "|-------------------|" + colores.RESET);
    System.out.println(colores.BLUE +   "    --Marcador--    " + colores.RESET);
    System.out.println(colores.PURPLE + "|-------------------|" + colores.RESET);
    System.out.println(colores.YELLOW + " Máquina= " + maquina + colores.RESET);
    System.out.println(colores.PURPLE + "|- - - - - - - - - -|" + colores.RESET);

    System.out.println(colores.BLUE +   " Persona= " + persona + colores.RESET);
    System.out.println(colores.PURPLE + "|- - - - - - - - - -|" + colores.RESET);

    if (print_empate){
    System.out.println(colores.YELLOW + " Empates= " + empate + colores.RESET);
    System.out.println(colores.PURPLE + "|-------------------|" + colores.RESET);
    System.out.println(colores.PURPLE + "|___________________|" + colores.RESET);

    }
  }

  public void reset(){
    maquina = 0;
    persona = 0;
    empate = 0;
  }


}
