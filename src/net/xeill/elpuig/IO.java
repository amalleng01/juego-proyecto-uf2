package net.xeill.elpuig;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

class IO {
  Scanner input = new Scanner(System.in);
  Colores colores = new Colores();


  public void mainMenu() {
    System.out.println(colores.GREEN+   "  --Juego de Alejandro Mallen--  "+colores.RESET);
    System.out.println(colores.GREEN+   " ________________________________"+colores.RESET);
    System.out.println(colores.GREEN+   "|--------------------------------|"+colores.RESET);
    System.out.println(colores.BLUE+    "| 1. Piedra, Papel o Tijera      |"+colores.RESET);
    System.out.println(colores.PURPLE + "| 2. Cara o cruz                 |"+colores.RESET);
    System.out.println(colores.PURPLE+  "| 3. Ahorcado                    |" + colores.RESET);
    System.out.println(colores.BLUE+    "| 4. Salir                       |"+colores.RESET);
    System.out.println(colores.GREEN+   "|--------------------------------|"+colores.RESET);
    System.out.println(colores.GREEN+   "|________________________________|"+colores.RESET);
  }

  public int selectMainMenu() {
    System.out.println(colores.YELLOW+"Escribe una de las opciones"+colores.RESET);
    int opcion;
    String soption;
    try {
      soption = input.nextLine();
      opcion = Integer.parseInt(soption);

    } catch (Exception e) {

      sleep(2);
      clearScreen();
      mainMenu();
      return selectMainMenu();
      // Hay que decidir qué hacer

    }
    return opcion;
  }

  /*
  Limpia la pantalla
  */
  void clearScreen(){
    System.out.print("\033\143");
  }

  /*
  Sleep
  */
  public void sleep(int seconds){
    try{
      TimeUnit.SECONDS.sleep(seconds);
    } catch(Exception e){


    }
  }

  public void menuAhorcado(Marcador marcador){
    System.out.println(colores.BLUE+"Has seleccionado jugar a Ahorcado"+colores.RESET);
    System.out.println(colores.RED+"-------------------------------------------------"+colores.RESET);
    //marcador en falso porque no tiene que imprimir empate
    marcador.imprimir(false);
    System.out.println(colores.YELLOW+"Comienza el juego"+colores.RESET);
  }

  public void menuCOC(Marcador marcador) {
    System.out.println(colores.GREEN+"Has seleccionado jugar a Cara o Cruz"+colores.RESET);
    System.out.println(colores.YELLOW+"-------------------------------------------------"+colores.RESET);
    marcador.imprimir(false);
    System.out.println(" ");
    System.out.println(colores.BLUE+"Comienza el juego"+colores.RESET);
  }

  public char pedirLetra(){
    System.out.println(colores.PURPLE+"Dime una letra"+colores.RESET);
    String letra = input.nextLine();
    if(letra.length() > 1){
      System.out.println(colores.BLUE+"Solo una letra!!!"+colores.RESET);
      return pedirLetra();
    }else if(letra.length() == 0){
      return pedirLetra();
    }else {return letra.charAt(0);}
  }

}
