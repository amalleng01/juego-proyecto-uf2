package net.xeill.elpuig;

import java.util.Scanner;

class PPT {

  Marcador marcador = new Marcador();
  IO io = new IO();
  Colores colores = new Colores();

  public void start() {
    System.out.println(colores.GREEN + "Has seleccionado jugar a Piedra, Papel o Tijera" + colores.RESET);
    System.out.println(colores.BLUE + "-------------------------------------------------" + colores.RESET);
    marcador.imprimir(true);
    System.out.println(colores.YELLOW + "Comienza el juego" + colores.RESET);
  }

  public int player() {
    Scanner input = new Scanner(System.in);
    int seleccion_usuario = 1;
    System.out.println(colores.YELLOW + "Indica piedra papel o tijera" + colores.RESET + "(1,2 o 3)" );

    try {
      String seleccion_usuario_string = input.nextLine();
      seleccion_usuario = Integer.parseInt(seleccion_usuario_string);
      /*
      soption = input.nextLine();
      opcion = Integer.parseInt(soption);
      */
      if(seleccion_usuario !=1 && seleccion_usuario !=2 && seleccion_usuario !=3){
        io.clearScreen();
        return player();
      }
    } catch (Exception e) {
        io.clearScreen();
      return player();
    }
    return seleccion_usuario;

  }


    public int machine() {

      System.out.println(colores.BLUE + "La máquina está escogiendo..." + colores.RESET);
      return (int)(Math.random()*3)+1; //Escoge un número del 0 al 2 pero le suma 1 así las opciones son 123

    }

    public void result(int seleccion_usuario, int seleccion_pc) {
      System.out.println(colores.BLUE + "La máquina ha escogido: " + colores.RESET);
      switch ( seleccion_pc ) {
        case 1:
          System.out.println(colores.YELLOW + "Piedra" + colores.RESET);
          switch ( seleccion_usuario ) {
            case 1:
            io.sleep(2);
            System.out.println(colores.BLUE + "= = = =" + colores.RESET);
            System.out.println(colores.BLUE + "Empate" + colores.RESET);
            System.out.println(colores.BLUE + "= = = =" + colores.RESET);
            marcador.suma("empate");
            break;
            case 2:
            io.sleep(2);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            System.out.println(colores.GREEN + "Ganas" + colores.RESET);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            marcador.suma("persona");
            break;
            case 3:
            io.sleep(2);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            System.out.println(colores.RED + "Pierdes" + colores.RESET);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            marcador.suma("maquina");
            break;
          }
          break;

        case 2:
          System.out.println(colores.PURPLE + "Papel" + colores.RESET);
          switch ( seleccion_usuario ) {
            case 1:
            io.sleep(2);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            System.out.println(colores.RED + "Pierdes" + colores.RESET);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            marcador.suma("maquina");
            break;

            case 2:
            io.sleep(2);
            System.out.println(colores.BLUE + "= = = =" + colores.RESET);
            System.out.println(colores.BLUE + "Empate" + colores.RESET);
            System.out.println(colores.BLUE + "= = = =" + colores.RESET);
            marcador.suma("empate");
            break;

            case 3:
            io.sleep(2);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            System.out.println(colores.GREEN + "Ganas" + colores.RESET);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            marcador.suma("persona");
            break;
          }
          break;

        case 3:
          System.out.println(colores.PURPLE + "Tijeras" + colores.RESET);
          switch ( seleccion_usuario ){
            case 1:
            io.sleep(2);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            System.out.println(colores.GREEN + "Ganas" + colores.RESET);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            marcador.suma("persona");
            break;

            case 2:
            io.sleep(2);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            System.out.println(colores.RED + "Pierdes" + colores.RESET);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            marcador.suma("maquina");
            break;

            case 3:
            io.sleep(2);
            System.out.println(colores.BLUE + "= = = =" + colores.RESET);
            System.out.println(colores.BLUE + "Empate" + colores.RESET);
            System.out.println(colores.BLUE + "= = = =" + colores.RESET);
            marcador.suma("empate");
            break;
          }
          break;

      }

    }

}
