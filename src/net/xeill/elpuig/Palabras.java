package net.xeill.elpuig;

import java.util.*;

public class Palabras {
ArrayList<String> listaPalabras = new ArrayList<String>();


  public void init(){
    listaPalabras.add("Rusben");
    listaPalabras.add("Alejandro");
  }

  public String seleccionarPalabra(int i){
    return listaPalabras.get(i);
  }

  public String seleccionarPalabraRandom(){
  int a = (int)(Math.random()*listaPalabras.size());
  return seleccionarPalabra(a);
  }

}
