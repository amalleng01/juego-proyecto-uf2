package net.xeill.elpuig;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Juegos2020{

    public static void main(String[] args) {
      run();
    }

    public static void run() {
      Scanner input = new Scanner(System.in);

      IO io = new IO();
      PPT ppt = new PPT();
      COC coc = new COC();
      Ahorcado ahorcado = new Ahorcado();
      Colores colores = new Colores();

      boolean salir = false;
      int opcion = 0; //Guardaremos la opcion del usuario

      while (!salir) {

          io.mainMenu();


              opcion = io.selectMainMenu();


              switch (opcion) {
                  case 1:
                      boolean ppti = true;

                      while (ppti == true) {
                        io.clearScreen();
                        ppt.start();
                      //  int seleccion_usuario = ppt.player();
                      //  int seleccion_pc = ppt.machine();
                      //  ppt.result(seleccion_usuario, seleccion_pc);
                        ppt.result(ppt.player(), ppt.machine());

                        System.out.println(colores.BLUE + "Quieres salir de este juego?:"+colores.RESET + "[si/no]" );
                        String salir_juego1 = input.nextLine();

                      //  System.out.println(salir_juego1);

                        if (salir_juego1.equals("si") ) {
                          io.clearScreen();
                          System.out.println(colores.PURPLE + "== == == == == == ==" + colores.RESET);
                          System.out.println("De vuelta al menu...");
                          System.out.println(colores.PURPLE + "== == == == == == ==" + colores.RESET);
                          io.sleep(2);
                          io.clearScreen();
                          ppt.marcador.reset();
                          ppti = false;
                        }
                      }
                      break;

                  case 2:

                      boolean coci = true;

                      while (coci == true) {
                        io.clearScreen();
                        coc.init();
                      //  int seleccion_usuario = ppt.player();
                      //  int seleccion_pc = ppt.machine();
                      //  ppt.result(seleccion_usuario, seleccion_pc);
                        coc.result(coc.player(), coc.machine());

                        System.out.println(colores.BLUE + "Quieres salir de este juego?:"+colores.RESET + "[si/no]" );
                        String salir_juego2 = input.nextLine();

                      //  System.out.println(salir_juego1);

                        if (salir_juego2.equals("si") ) {
                          io.clearScreen();
                          System.out.println(colores.PURPLE + "== == == == == == ==" + colores.RESET);
                          System.out.println("De vuelta al menu...");
                          System.out.println(colores.PURPLE + "== == == == == == ==" + colores.RESET);
                          io.sleep(2);
                          io.clearScreen();
                          coc.marcador.reset();
                          coci = false;
                        } else {

                        }
                      }
                      break;





                  case 3:

                      boolean ah = true;

                      while (ah == true){
                        io.clearScreen();
                        ahorcado.init();

                        System.out.println(colores.BLUE + "Quieres salir de este juego?:"+colores.RESET + "[si/no]" );
                        String salir_juego3 = input.nextLine();

                        if (salir_juego3.equals("si") ) {
                          io.clearScreen();
                          System.out.println(colores.PURPLE + "== == == == == == ==" + colores.RESET);
                          System.out.println("De vuelta al menu...");
                          System.out.println(colores.PURPLE + "== == == == == == ==" + colores.RESET);

                          io.sleep(2);
                          io.clearScreen();
                          ahorcado.marcador.reset();
                          ah = false;
                        }
                      }
                      break;

                  case 4:
                      io.clearScreen();


                      System.out.println(colores.YELLOW + "~ ~ ~" + colores.RESET);
                      System.out.println(colores.YELLOW + "Adios" + colores.RESET);
                      System.out.println(colores.YELLOW + "~ ~ ~" + colores.RESET);

                      
                      io.sleep(2);

                      //salir del bucle y salir del programa
                      salir = true;

                      break;
                  default:
                      io.clearScreen();
                      System.out.println(colores.PURPLE + "Solo números entre 1 y 4" + colores.RESET);
              }
          // } catch (Exception e) {
          //     System.out.println("Debes insertar un número");
          //     run();
          // }
      }

    }

}
