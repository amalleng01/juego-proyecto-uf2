package net.xeill.elpuig;

import java.util.*;



public class Asteriscos {
  //limite de fallos totales por largo sea la palabra
  int fallosTotales = 0;


  //contador
  int fallos = 0;

  //Array letras conocidas/acertadas
  ArrayList<Character> letrasConocidas = new ArrayList<Character>();

  public void init(String palabra){
    fallosTotales = palabra.length()+3;
  }




//La destapa si es conocida y la compara con mayus
  public boolean esConocida(char letra){
    for(char c : letrasConocidas){
      if(c == Character.toUpperCase(letra)){
        return true;
      }
    }
    //no encuentra la letra
    return false;
  }

//Si la letra esta en la palabra o no
  public boolean estaPalabra(char letra, String palabra){
    char[] letras = palabra.toCharArray();

    for(int i = 0; i < letras.length; i++){
      if(letra == letras[i]){
      return true;
    }

  }
  return false;
}

//convierte las letras en asteriscos, pero si esta las imprime
public void desguazar(String palabra){
  char[] letras = palabra.toCharArray();

  for (int i = 0; i < letras.length; i++){
    if(esConocida(letras[i])){
      System.out.print(letras[i]);
    }
    else{System.out.print("*");}
    }
System.out.println();
}

//Añade las letras en mayusculas a letrasConocidas
public void anyadirLetrasC(char letra){
  letrasConocidas.add(Character.toUpperCase(letra));
}



public void incrementarFallos(){

  fallos++;
}

public boolean gameOver(){
  if(fallos == fallosTotales){

    return true;
  }
  else{return false;}
}

public void resetFallos(){
  this.fallos = 0;
}

//Reinicia las palabras para que cuando vuelvas a jugar sigan tapadas
public void resetConocida(){
  letrasConocidas.clear();
}



public boolean checkWin(String palabra){
  for(int i = 0; i < palabra.length(); i++){
    if(!esConocida(palabra.charAt(i))){
      return false;
    }
  }
  //conoces todas las letras y ganas
  return true;
}






}
