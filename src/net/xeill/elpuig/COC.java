package net.xeill.elpuig;

import java.util.Scanner;

class COC {

  Marcador marcador = new Marcador();
  IO io = new IO();
  Colores colores = new Colores();

  public void init(){
    io.menuCOC(marcador);
  }

  public int player() {

    Scanner input = new Scanner(System.in);
    int seleccion_usuario = 1;
    System.out.println(colores.YELLOW + "Indica  cara o cruz " + colores.RESET + "(Cara = 1 Cruz = 2)" );

    //Lee toda la linea y lo pasa a int para que se registre algun valor. Directamente con int se buugea
    try {
      String seleccion_usuario_string = input.nextLine();
      seleccion_usuario = Integer.parseInt(seleccion_usuario_string);

      if(seleccion_usuario !=1 && seleccion_usuario !=2){
        io.clearScreen();
        return player();
      }

    } catch (Exception e) {
      io.clearScreen();
      return player();
    }
    return seleccion_usuario;
  }

    public int machine() {

      System.out.println(colores.GREEN + "La máquina está escogiendo..." + colores.RESET);
      io.sleep(2);
      return (int)(Math.random()*2)+1; //Escoge un número del 0 al 2

    }

    public void result(int seleccion_usuario, int seleccion_pc) {
      System.out.println(colores.BLUE + " La máquina ha escogido: " + colores.RESET);
      switch ( seleccion_pc ) {
        case 1:
          System.out.println(colores.YELLOW + "Cara" + colores.RESET);
          switch ( seleccion_usuario ) {
            case 1:
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            System.out.println(colores.GREEN + "Ganas" + colores.RESET);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            marcador.suma("persona");
            break;

            case 2:
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            System.out.println(colores.RED + "Pierdes" + colores.RESET);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            marcador.suma("maquina");
            break;
          }
          break;

        case 2:
          System.out.println(colores.BLUE + "Cruz" + colores.RESET);
          switch ( seleccion_usuario ) {
            case 1:
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            System.out.println(colores.RED + "Pierdes" + colores.RESET);
            System.out.println(colores.RED + "= = = =" + colores.RESET);
            marcador.suma("maquina");
            break;

            case 2:
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            System.out.println(colores.GREEN + "Ganas" + colores.RESET);
            System.out.println(colores.GREEN + "= = =" + colores.RESET);
            marcador.suma("persona");
            break;
          }
          break;

        }
    }


}
