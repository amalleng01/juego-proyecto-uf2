package net.xeill.elpuig;

import java.util.*;


public class Ahorcado {

  Marcador marcador = new Marcador();
  IO io = new IO();
  Palabras palabras = new Palabras();
  Asteriscos asteriscos = new Asteriscos();
  Colores colores = new Colores();






  public void init(){
    Scanner input = new Scanner(System.in);


    io.menuAhorcado(marcador);

    palabras.init();
    String palabra = palabras.seleccionarPalabraRandom();
    asteriscos.init(palabra);
    System.out.println("Palabra aleatoria generada: ");

    boolean acabar = false;
    while(!acabar){

      asteriscos.desguazar(palabra);


      char letra = io.pedirLetra();

      if(asteriscos.estaPalabra(letra, palabra)){

        asteriscos.anyadirLetrasC(letra);
      }
      else{asteriscos.incrementarFallos();
          System.out.println("Número de intentos restantes: " + (asteriscos.fallosTotales - asteriscos.fallos));

      asteriscos.anyadirLetrasC(letra);
      }

      if(asteriscos.checkWin(palabra)){
        marcador.suma("persona");
        asteriscos.resetFallos();
        asteriscos.resetConocida();
        System.out.println(colores.GREEN + "= = = = " + colores.RESET);
        System.out.println(colores.GREEN + "Victoria" + colores.RESET);
        System.out.println(colores.GREEN + "= = = = " + colores.RESET);

        acabar = true;
      }

      if (asteriscos.gameOver()){
        marcador.suma("maquina");
        System.out.println(colores.RED + "= = = =" + colores.RESET);
        System.out.println(colores.RED + "Derrota" + colores.RESET);
        System.out.println(colores.RED + "= = = =" + colores.RESET);
        asteriscos.resetFallos();
        asteriscos.resetConocida();

        acabar = true;
      }




    }







  }




}
